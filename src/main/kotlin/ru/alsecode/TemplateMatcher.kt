package ru.alsecode

import org.bytedeco.javacpp.DoublePointer
import org.bytedeco.opencv.global.opencv_core
import org.bytedeco.opencv.global.opencv_imgcodecs
import org.bytedeco.opencv.global.opencv_imgproc
import org.bytedeco.opencv.opencv_core.Mat
import org.bytedeco.opencv.opencv_core.Point
import org.bytedeco.opencv.opencv_core.Scalar
import java.io.File

fun main(args: Array<String>) {
    if (args.size < 3) {
        println("Please, set args: first - image path, second - template path, third - output image path")
        return
    }

    val image = File(args[0]).loadAsMat()
    val template = File(args[1]).loadAsMat()

    val (imgReport, accuracy) = TemplateMatcher(opencv_imgproc.TM_CCOEFF_NORMED).match(image, template)
    opencv_imgcodecs.imwrite(args[2], imgReport)

    println("Done! Accuracy: $accuracy%. Check " + args[2])
}

class TemplateMatcher(private val matchMethod: Int) {
    init {
        if (matchMethod !in arrayOf(
                opencv_imgproc.TM_CCORR_NORMED,
                opencv_imgproc.TM_CCOEFF_NORMED,
                opencv_imgproc.TM_SQDIFF_NORMED
            )
        ) throw RuntimeException("unknown match method $matchMethod")
    }

    fun match(img: Mat, template: Mat): Pair<Mat, Double> {
        val matchReport = Mat()
        opencv_imgproc.matchTemplate(img, template, matchReport, matchMethod)
//            opencv_core.normalize(result, result, 0, 1, opencv_core.NORM_MINMAX, -1, new Mat()) - если бы был matchMethod без _NORMED

        val minVal = DoublePointer(1)
        val maxVal = DoublePointer(1)
        val minLoc = Point()
        val maxLoc = Point()
        opencv_core.minMaxLoc(matchReport, minVal, maxVal, minLoc, maxLoc, null)
        val matchLoc = if (matchMethod == opencv_imgproc.TM_SQDIFF_NORMED) minLoc else maxLoc

        val result = img.clone()
        opencv_imgproc.rectangle(
            result,
            matchLoc,
            Point(matchLoc.x() + template.cols(), matchLoc.y() + template.rows()),
            Scalar(200.0, 200.0, 0.0, 0.0), 2, 8, 0
        )
        return result to getAccuracyInPercent(minVal.get(), maxVal.get())
    }

    private fun getAccuracyInPercent(minVal: Double, maxVal: Double) =
        100.0 * if (matchMethod == opencv_imgproc.TM_SQDIFF_NORMED) 1 - minVal else maxVal
}
