package ru.alsecode

import org.bytedeco.opencv.global.opencv_imgcodecs
import org.bytedeco.opencv.opencv_core.Mat
import java.io.File


fun File.loadAsMat(): Mat {
    val result = opencv_imgcodecs.imread(absolutePath, opencv_imgcodecs.IMREAD_COLOR)
    require(!result.empty()) { "Error loading file '$absolutePath' (Is there non-latin symbols in path?)" }
    return result
}
