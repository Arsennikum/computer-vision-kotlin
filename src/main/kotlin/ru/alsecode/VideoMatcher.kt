package ru.alsecode

import org.bytedeco.javacv.FFmpegFrameGrabber
import org.bytedeco.javacv.FrameGrabber
import org.bytedeco.javacv.OpenCVFrameConverter
import org.bytedeco.opencv.global.opencv_imgproc
import org.bytedeco.opencv.opencv_core.Mat
import org.bytedeco.opencv.opencv_core.Point
import org.bytedeco.opencv.opencv_core.Scalar
import org.bytedeco.opencv.opencv_core.Size
import org.bytedeco.opencv.opencv_videoio.VideoWriter
import java.io.File

private val converterToMat = OpenCVFrameConverter.ToMat()

fun main(args: Array<String>) {
    if (args.size < 3) {
        println("Please, set args: first - video path, second - template path, third - output video path")
        return
    }

    val template = File(args[1]).loadAsMat()
    val grabber = FFmpegFrameGrabber(args[0]).also(FrameGrabber::start)

    val outputVideo = args[2] + ".avi"
    val videoWriter = VideoWriter(
        outputVideo,
        VideoWriter.fourcc('M'.toByte(), 'J'.toByte(), 'P'.toByte(), 'G'.toByte()),
        grabber.videoFrameRate,
        Size(grabber.imageWidth, grabber.imageHeight)
    )

    var i = 0
    var frame = grabber.grabImage()
    while (frame != null) {
        val frameMat = converterToMat.convert(frame)
        val (resFrame, accuracy) = TemplateMatcher(opencv_imgproc.TM_CCOEFF_NORMED).match(frameMat, template)
        drawFrameNumber(resFrame, i)
        drawAccuracy(resFrame, accuracy)

        videoWriter.write(resFrame)

        frame = grabber.grabImage()
        i++
    }
    grabber.release()
    grabber.close()
    videoWriter.release()
    videoWriter.close()

    println("Done! Check " + args[2])
}

private fun drawFrameNumber(image: Mat, number: Int) = drawText(image, "$number", Point(image.cols() / 3 - 100, 50))

private fun drawAccuracy(image: Mat, accuracy: Double) =
    drawText(image, "Accuracy: $accuracy", Point(image.cols() / 3, image.rows() - 30))

private fun drawText(image: Mat, text: String, coordinate: Point) {
    opencv_imgproc.putText(
        image,
        text,
        coordinate,
        0,
        1.2,
        Scalar(0.0, 0.0, 0.0, 0.0),
        4,
        opencv_imgproc.LINE_AA,
        false
    )
}
